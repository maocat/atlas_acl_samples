EN|[CN](README.md)
# Atlas ACL Samples

Atlas ACL samples enables users to understand the development of Atlas ACL in more detail through a series of function development samples.

## Table of Contents
### 1. cplusplus

#### 1.1 Image & Video Processing Samples
- Table

    | directory  | explaination  |
    |:---:|:---|
    | [compile_demo](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/compile_demo)  | This sample demonstrates compile_demo program. <br> Process Framework： <br> Create context and stream -> memory copy -> Destory context and stream  |
    | [decode_image](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/decode_image)  | This sample demonstrates how the decode_image program to use the chip. <br> Process Framework： <br> ReadJpeg -> JpegDecode -> ImageResize -> WriteResult  |
    | [decode_video](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/decode_video)  | This sample demonstrates the decode_video program, describing the way of video decoding using the chip. <br> Process Framework:  <br> ReadFile > VdecDecode > WriteResult  |
    | [dvpp_crop](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/dvpp_crop)  | This sample demonstrates the dvpp_crop program, showing the way to use VPC module for image cropping. Only YUV420 format is supported. Cropped image will be saved to a YUV format file. <br> Process Framework： <br> ReadYUV -> DvppCrop -> SaveFile  |
    | [encode_jpeg](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/encode_jpeg)  | This sample demonstrates how the encode_jpeg program to use dvpp to Encode a YUV file into a Jpeg image. <br> Process Framework： <br> ReadYUV -> EncodeJpeg -> SaveFile  |
    | [multi_video_decode](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/multi_video_decode)  | This sample demonstrates how the multi_video_decode program to use dvpp to Multi channel video decoding. <br> Process Framework： <br> StreamPuller > VideoDecoder > WriteResult  |
    | [multi_video_transcoding](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/multi_video_transcoding)  | This sample demonstrates how the multi_video_transcoding program to use dvpp to Multi channel video Transcoding. <br> Process Framework： <br> StreamPuller > VideoDecoder > ImageResize > VideoEncode > WriteResult |

#### 1.2 Infer Image Classification
- Table

    | directory  | model  | frame  | explaination  |
    |:---:|:---:|:---:|:---|
    | [image_resnet50](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/classification/image_resnet50)  | Resnet50 | TensorFlow | This sample demonstrates how the image_resnet50 program to use the chip to perform the 'resnet50' target classification. <br> Process Framework: <br> ReadJpeg > JpegDecode > ImageResize > ObjectClassification > Cast_Op > ArgMax_Op > WriteResult  |

#### 1.3 Infer Image & Video ObjectDetection
- Table

    | directory  | model  | frame  | explaination  |
    |:---:|:---:|:---:|:---|
    | [image_yolov3v4v5](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/object_detection/image_yolov3v4v5)  | YoloV3 <br> YoloV4 <br> YoloV5 | Caffe <br> TensorFlow <br> Pytorch | This sample demonstrates how the image_yolov3v4v5 program to use the chip to perform the 'YoloV3' object detection. <br> Process Framework：<br> ReadJpeg > JpegDecode > ImageResize > ObjectDetection > YoloV3PostProcess > WriteResult  |
    | [video_yolov3v4v5](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/object_detection/video_yolov3v4v5)  | YoloV3 <br> YoloV4 <br> YoloV5 | Caffe <br> TensorFlow <br> Pytorch | This sample demonstrates how the video_yolov3v4v5 program to use the chip to perform the 'YoloV3' object detection. <br> Process Framework：<br> StreamPuller > VideoDecoder > ObjectDetection > PostProcess > WriteResult  |

### 2. Python