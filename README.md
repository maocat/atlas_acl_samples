中文|[英文](README.en.md)
# Atlas ACL Samples

Atlas ACL Samples通过一系列功能的开发样例，使用户更详细地了解Atlas ACL的开发。

## Table of Contents

### 1. cplusplus

#### 1.1 Image & Video Processing Samples
- Table

    | 目录  | 说明  |
    |:---:|:---|
    | [compile_demo](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/compile_demo)  | 本开发样例演示 compile_demo 程序。 <br> 该Sample处理流程为： <br> 创建 context、stream -> 内存复制 -> 销毁 context、stream  |
    | [decode_image](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/decode_image)  | 本开发样例演示 decode_image 程序，使用芯片进行 解码、缩放。 <br> 该Sample处理流程为： <br> ReadJpeg -> JpegDecode -> ImageResize -> WriteResult  |
    | [decode_video](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/decode_video)  | 本开发样例演示 decode_video 程序，使用芯片进行视频文件解码。 <br> 该Sample的处理流程为:  <br> ReadFile > VdecDecode > WriteResult  |
    | [dvpp_crop](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/dvpp_crop)  | 本开发样例演示 dvpp_crop 程序，示例如何使用 Dvpp 的 VPC 模块进行抠图，支持从一张输入 YUV420 格式的图片中扣取其中一部分，保存到 YUV 格式的文件。 <br> 该Sample处理流程为： <br> ReadYUV -> DvppCrop -> SaveFile  |
    | [encode_jpeg](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/encode_jpeg)  | 本开发样例演示 encode_jpeg 程序，使用芯片dvpp进行Jpeg编码。 <br> 该Sample处理流程为： <br> ReadYUV -> EncodeJpeg -> SaveFile  |
    | [multi_video_decode](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/multi_video_decode)  | 本开发样例演示 multi_video_decode 程序，使用芯片dvpp进行多路视频解码。 <br> 该Sample处理流程为： <br> StreamPuller > VideoDecoder > WriteResult  |
    | [multi_video_transcoding](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/data_process/multi_video_transcoding)  | 本开发样例演示 multi_video_transcoding 程序，使用芯片dvpp进行多路视频转码。 <br> 该Sample处理流程为： <br> StreamPuller > VideoDecoder > ImageResize > VideoEncode > WriteResult  |

#### 1.2 Infer Image Classification
- Table

    | 目录  | 模型  | 框架  | 说明  |
    |:---:|:---:|:---:|:---|
    | [image_resnet50](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/classification/image_resnet50)  | Resnet50 | TensorFlow | 本开发样例演示 image_resnet50 程序，使用芯片进行 resnet50 目标分类。<br> 该Sample的处理流程为: <br> ReadJpeg > JpegDecode > ImageResize > ObjectClassification > Cast_Op > ArgMax_Op > WriteResult  |

#### 1.3 Infer Image & Video ObjectDetection
- Table

    | 目录  | 模型  | 框架  | 说明  |
    |:---:|:---:|:---:|:---|
    | [image_yolov3v4v5](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/object_detection/image_yolov3v4v5)  | YoloV3 <br> YoloV4 <br> YoloV5 | Caffe <br> TensorFlow <br> Pytorch | 本开发样例演示 image_yolov3v4v5 程序，使用芯片进行 YoloV3/YoloV4/YoloV5 目标识别。<br> 该Sample的处理流程为：<br> ReadJpeg > JpegDecode > ImageResize > ObjectDetection > YoloV3PostProcess > WriteResult  |
    | [video_yolov3v4v5](https://gitee.com/ai_samples/atlas_acl_samples/tree/master/cpp/contrib/cv/object_detection/video_yolov3v4v5)  | YoloV3 <br> YoloV4 <br> YoloV5 | Caffe <br> TensorFlow <br> Pytorch | 本开发样例演示 video_yolov3v4v5 程序，使用芯片进行 YoloV3/YoloV4/YoloV5 目标识别。<br> 该Sample的处理流程为：<br> StreamPuller > VideoDecoder > ObjectDetection > PostProcess > WriteResult  |

### 2. Python