/*
 * Copyright(C) 2020. Huawei Technologies Co.,Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef USE_DCMI_INTERFACE
#include <unistd.h>
#include <termios.h>
#endif
#include "ModelProcess.h"
#include "FileManager/FileManager.h"

ModelProcess::ModelProcess(const int deviceId, const std::string& modelName)
{
    deviceId_ = deviceId;
    modelName_ = modelName;
}

ModelProcess::ModelProcess() {}

ModelProcess::~ModelProcess()
{
    if (!isDeInit_) {
        DeInit();
    }
}

void ModelProcess::DestroyDataset(const aclmdlDataset *dataset) const
{
    // Just release the DataBuffer object and DataSet object, remain the buffer, because it is managerd by user
    if (dataset != nullptr) {
        for (size_t i = 0; i < aclmdlGetDatasetNumBuffers(dataset); i++) {
            aclDataBuffer* dataBuffer = aclmdlGetDatasetBuffer(dataset, i);
            if (dataBuffer != nullptr) {
                aclDestroyDataBuffer(dataBuffer);
                dataBuffer = nullptr;
            }
        }
        aclmdlDestroyDataset(dataset);
        dataset = nullptr;
    }
}

aclmdlDesc *ModelProcess::GetModelDesc() const
{
    return modelDesc_.get();
}

APP_ERROR ModelProcess::SetDynamicInfo(aclmdlDataset *input)
{
    if (dynamicInfo_.dynamicType == STATIC_BATCH) {
        return APP_ERR_OK;
    }

    size_t index;
    APP_ERROR ret = aclmdlGetInputIndexByName(modelDesc_.get(), ACL_DYNAMIC_TENSOR_NAME, &index);
    if (ret != ACL_ERROR_NONE) {
        LogError << "aclmdlGetInputIndexByName failed, maybe static model";
        return APP_ERR_COMM_CONNECTION_FAILURE;
    }

    switch (dynamicInfo_.dynamicType) {
    case STATIC_BATCH:
        break;
    case DYNAMIC_BATCH:
        CHECK_RET_EQ(SetDynamicBatchSize(input, index, dynamicInfo_.dyBatch.batchSize), APP_ERR_OK);
        break;
    case DYNAMIC_HW:
        CHECK_RET_EQ(SetDynamicHW(input, index, dynamicInfo_.dyHW.height, dynamicInfo_.dyHW.width), APP_ERR_OK);
        break;
    case DYNAMIC_DIMS:
        if (dynamicInfo_.dyDims.pDims == nullptr){
            LogWarn << "error dynamic dims type but pdims is null";
        }else{
            CHECK_RET_EQ(SetDynamicDims(input, index, dynamicInfo_.dyDims.pDims->dym_dims), APP_ERR_OK);
        }
        break;
    case DYNAMIC_SHAPE:
        if (dynamicInfo_.dyShape.pShapes == nullptr){
            LogWarn << "error dynamic shapes type but pshapes is null";
        }else{
            CHECK_RET_EQ(SetDynamicShape(input, 
                dynamicInfo_.dyShape.pShapes->dym_shape_map, dynamicInfo_.dyShape.pShapes->dims_num), APP_ERR_OK);
        }
        break;
    }

    return APP_ERR_OK;
}

int ModelProcess::ModelInference(const std::vector<void *> &inputBufs, const std::vector<size_t> &inputSizes,
    const std::vector<void *> &ouputBufs, const std::vector<size_t> &outputSizes)
{
    LogDebug << "ModelProcess:Begin to inference.";
    aclmdlDataset *input = nullptr;
    input = CreateAndFillDataset(inputBufs, inputSizes);
    if (input == nullptr) {
        return APP_ERR_COMM_FAILURE;
    }

    APP_ERROR ret = SetDynamicInfo(input);
    if (ret != APP_ERR_OK) {
        LogError << "SetDynamicInfo failed, ret[" << ret << "].";
        return ret;
    }

    aclmdlDataset *output = nullptr;
    output = CreateAndFillDataset(ouputBufs, outputSizes);
    if (output == nullptr) {
        DestroyDataset(input);
        input = nullptr;
        return APP_ERR_COMM_FAILURE;
    }
    mtx_.lock();
    ret = aclmdlExecute(modelId_, input, output);
    mtx_.unlock();
    if (ret != APP_ERR_OK) {
        LogError << "aclmdlExecute failed, ret[" << ret << "].";
        return ret;
    }

    DestroyDataset(input);
    DestroyDataset(output);
    return APP_ERR_OK;
}

int ModelProcess::DeInit()
{
    LogInfo << "Model[" << modelName_ << "][" << deviceId_ << "] deinit begin";
    isDeInit_ = true;
    APP_ERROR ret = aclmdlUnload(modelId_);
    if (ret != APP_ERR_OK) {
        LogError << "aclmdlUnload  failed, ret["<< ret << "].";
        return ret;
    }

    if (modelDevPtr_ != nullptr) {
        ret = aclrtFree(modelDevPtr_);
        if (ret != APP_ERR_OK) {
            LogError << "aclrtFree  failed, ret[" << ret << "].";
            return ret;
        }
        modelDevPtr_ = nullptr;
    }
    if (weightDevPtr_ != nullptr) {
        ret = aclrtFree(weightDevPtr_);
        if (ret != APP_ERR_OK) {
            LogError << "aclrtFree  failed, ret[" << ret << "].";
            return ret;
        }
        weightDevPtr_ = nullptr;
    }
    for (size_t i = 0; i < inputBuffers_.size(); i++) {
        if (inputBuffers_[i] != nullptr) {
            aclrtFree(inputBuffers_[i]);
            inputBuffers_[i] = nullptr;
        }
    }

    for (size_t i = 0; i < outputBuffers_.size(); i++) {
        if (outputBuffers_[i] != nullptr) {
            aclrtFree(outputBuffers_[i]);
            outputBuffers_[i] = nullptr;
        }
    }
    inputSizes_.clear();
    outputSizes_.clear();
    LogInfo << "Model[" << modelName_ << "][" << deviceId_ << "] deinit success";
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::LoadModel(const std::shared_ptr<uint8_t> &modelData, int modelSize)
{
    APP_ERROR ret = aclmdlQuerySizeFromMem(modelData.get(), modelSize, &modelDevPtrSize_, &weightDevPtrSize_);
    if (ret != APP_ERR_OK) {
        LogError << "aclmdlQuerySizeFromMem failed, ret[" << ret << "].";
        return ret;
    }
    LogDebug << "modelDevPtrSize_[" << modelDevPtrSize_ << "], weightDevPtrSize_[" << weightDevPtrSize_ << "].";

    ret = aclrtMalloc(&modelDevPtr_, modelDevPtrSize_, ACL_MEM_MALLOC_HUGE_FIRST);
    if (ret != APP_ERR_OK) {
        LogError << "aclrtMalloc dev_ptr failed, ret[" << ret << "].";
        return ret;
    }
    ret = aclrtMalloc(&weightDevPtr_, weightDevPtrSize_, ACL_MEM_MALLOC_HUGE_FIRST);
    if (ret != APP_ERR_OK) {
        LogError << "aclrtMalloc weight_ptr failed, ret[" << ret << "] (" << GetAppErrCodeInfo(ret) << ").";
        return ret;
    }
    ret = aclmdlLoadFromMemWithMem(modelData.get(), modelSize, &modelId_, modelDevPtr_, modelDevPtrSize_,
        weightDevPtr_, weightDevPtrSize_);
    if (ret != APP_ERR_OK) {
        LogError << "aclmdlLoadFromMemWithMem failed, ret[" << ret << "].";
        return ret;
    }
    ret = aclrtGetCurrentContext(&contextModel_);
    if (ret != APP_ERR_OK) {
        LogError << "aclrtMalloc weight_ptr failed, ret[" << ret << "].";
        return ret;
    }
    // get input and output size
    aclmdlDesc *modelDesc = aclmdlCreateDesc();
    if (modelDesc == nullptr) {
        LogError << "aclmdlCreateDesc failed.";
        return ret;
    }
    ret = aclmdlGetDesc(modelDesc, modelId_);
    if (ret != APP_ERR_OK) {
        LogError << "aclmdlGetDesc ret fail, ret:" << ret << ".";
        return ret;
    }
    modelDesc_.reset(modelDesc, aclmdlDestroyDesc);
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::Init(std::string modelPath)
{
    LogInfo << "ModelProcess:Begin to init instance.";
    int modelSize = 0;
    std::shared_ptr<uint8_t> modelData = nullptr;

    // modelPath should point to an encrypted model when isEncrypted is true
    APP_ERROR ret = ReadBinaryFile(modelPath, modelData, modelSize);
    if (ret != APP_ERR_OK) {
        LogError << "read model file failed, ret[" << ret << "].";
        return ret;
    }

    return LoadModel(modelData, modelSize);
}

#ifdef USE_DCMI_INTERFACE
void ModelProcess::SetConsoleDispMode(int fd, int option)
{
    struct termios term;
    if (tcgetattr(fd, &term) == -1) {
        LogWarn << "Failed to get the attribution of the terminal, errno=" << errno << ".";
        return;
    }

    const tcflag_t echoFlags = (ECHO | ECHOE | ECHOK | ECHONL);
    if (option) {
        term.c_lflag |= echoFlags;
    } else {
        term.c_lflag &= ~echoFlags;
    }
    int err = tcsetattr(fd, TCSAFLUSH, &term);
    if ((err == -1) || (err == EINTR)) {
        LogWarn << "Failed to set the attribution of the terminal, errno=" << errno << ".";
        return;
    }
    return;
}

/*
 * @description: Get id and password of secret key encrypted information
 * @return: APP_ERR_OK success
 * @return: Other values failure
 * @attention: This function needs to be implemented by users.
 */
APP_ERROR ModelProcess::GetKeyIdPassword(unsigned int &id, unsigned char password[], unsigned int &passwordLen) const
{
    LogInfo << "This function should be implemented by users.";

    LogInfo << "Please input secret key encryped index:";
    while (1) {
        std::cin >> id;
        if (std::cin.rdstate() == std::ios::goodbit) {
            // Clear newline character
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            break;
        }
        // Clear the cin state and buffer to receive the next input
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        LogInfo << "Input error, please input secret key encryped index again:";
    }

    LogInfo << "Please input secret key encryped password:";
    // Disable the terminal display when entering the password
    SetConsoleDispMode(STDIN_FILENO, 0);
    std::cin.get(reinterpret_cast<char *>(password), MAX_ENCODE_LEN);
    // Enable the terminal display when entering the password
    SetConsoleDispMode(STDIN_FILENO, 1);
    passwordLen = strlen(reinterpret_cast<char *>(password));
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::Init(const std::string &modelPath, bool isEncrypted, int cardId, int deviceId)
{
    if (!isEncrypted) {
        return Init(modelPath);
    }

    LogInfo << "ModelProcess:Begin to init instance.";
    int modelSize = 0;
    std::shared_ptr<uint8_t> modelData = nullptr;

    // modelPath should point to an encrypted model when isEncrypted is true
    APP_ERROR ret = ReadBinaryFile(modelPath, modelData, modelSize);
    if (ret != APP_ERR_OK) {
        LogError << "Failed to read model file, ret[" << ret << "].";
        return ret;
    }

    // Users need to implement this function as required
    ret = GetKeyIdPassword(encryptModelData_.id, encryptModelData_.password, encryptModelData_.password_len);
    if (ret != APP_ERR_OK) {
        return ret;
    }

    ret = dcmi_init();
    if (ret != APP_ERR_OK) {
        LogError << "Failed to initialize dcmi, ret = " << ret << ".";
        return ret;
    }

    // Read secret key from dcmi
    ret = dcmi_get_ai_model_info(cardId, deviceId, &encryptModelData_);
    if (ret != APP_ERR_OK) {
        LogError << "Failed to get model info from dcmi, ret[" << ret << "].";
        return ret;
    }

    // Clear password immediately after use
    aclrtMemset(encryptModelData_.password, sizeof(encryptModelData_.password), 0, sizeof(encryptModelData_.password));
    LogInfo << "Users need to decrypt model before the next operation.";

    // User should modify the decryptedModelData and encryptedModelSize according to the actual situation
    std::shared_ptr<uint8_t> decryptedModelData = modelData;
    int encryptedModelSize = modelSize;

    // Load decrypted model
    return LoadModel(decryptedModelData, encryptedModelSize);
}
#endif

aclmdlDataset *ModelProcess::CreateAndFillDataset(const std::vector<void *> &bufs, const std::vector<size_t> &sizes)
    const
{
    APP_ERROR ret = APP_ERR_OK;
    aclmdlDataset *dataset = aclmdlCreateDataset();
    if (dataset == nullptr) {
        LogError << "ACL_ModelInputCreate failed.";
        return nullptr;
    }

    for (size_t i = 0; i < bufs.size(); ++i) {
        aclDataBuffer *data = aclCreateDataBuffer(bufs[i], sizes[i]);
        if (data == nullptr) {
            DestroyDataset(dataset);
            LogError << "aclCreateDataBuffer failed.";
            return nullptr;
        }

        ret = aclmdlAddDatasetBuffer(dataset, data);
        if (ret != APP_ERR_OK) {
            DestroyDataset(dataset);
            LogError << "ACL_ModelInputDataAdd failed, ret[" << ret << "].";
            return nullptr;
        }
    }
    return dataset;
}

size_t ModelProcess::GetModelNumInputs() const
{
    return aclmdlGetNumInputs(modelDesc_.get());
}

size_t ModelProcess::GetModelNumOutputs() const
{
    return aclmdlGetNumOutputs(modelDesc_.get());
}

size_t ModelProcess::GetModelInputSizeByIndex(const size_t &i) const
{
    return aclmdlGetInputSizeByIndex(modelDesc_.get(), i);
}

APP_ERROR ModelProcess::GetDynamicIndex(size_t &dymindex)
{
    aclError ret; 

    const char *inputname = nullptr;
    bool dynamicIndex_exist = false;
    size_t numInputs = aclmdlGetNumInputs(modelDesc_.get());
    for (size_t i = 0; i < numInputs; i++) {
        inputname = aclmdlGetInputNameByIndex(modelDesc_.get(), i);
        if (strcmp(inputname, ACL_DYNAMIC_TENSOR_NAME) == 0){
            dynamicIndex_exist = true;
        }
    }
    if (dynamicIndex_exist == false){
        g_dymindex = -1;
        return APP_ERR_OK;
    }

    ret = aclmdlGetInputIndexByName(modelDesc_.get(), ACL_DYNAMIC_TENSOR_NAME, &dymindex);
    if (ret != ACL_SUCCESS) {
        std::cout << aclGetRecentErrMsg() << std::endl;
        LogError << "get input index by name failed " << ret;
        g_dymindex = -1;
        return ret;
    }
    LogDebug << "get input index by name success";
    g_dymindex = dymindex;
    return APP_ERR_OK;
}

size_t ModelProcess::GetModelOutputSizeByIndex(const size_t &i) const
{
    return aclmdlGetOutputSizeByIndex(modelDesc_.get(), i);
}

APP_ERROR ModelProcess::InputBufferWithSizeMalloc(aclrtMemMallocPolicy policy)
{
    size_t inputNum = aclmdlGetNumInputs(modelDesc_.get());
    LogDebug << modelName_ << "model inputNum is : " << inputNum << ".";
    for (size_t i = 0; i < inputNum; ++i) {
        void *buffer = nullptr;
        // modify size
        size_t size = aclmdlGetInputSizeByIndex(modelDesc_.get(), i);
        APP_ERROR ret = aclrtMalloc(&buffer, size, policy);
        if (ret != APP_ERR_OK) {
            LogFatal << modelName_ << "model input aclrtMalloc fail(ret=" << ret
                     << "), buffer=" << buffer << ", size=" << size << ".";
            // Free the buffer malloced successfully before return error
            ReleaseModelBuffer(inputBuffers_);
            return ret;
        }
        inputBuffers_.push_back(buffer);
        inputSizes_.push_back(size);
        LogDebug << modelName_ << "model inputBuffer i=" << i << ", size=" << size << ".";
    }
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::OutputBufferWithSizeMalloc(aclrtMemMallocPolicy policy)
{
    size_t outputNum = aclmdlGetNumOutputs(modelDesc_.get());
    LogDebug << modelName_ << "model outputNum is : " << outputNum << ".";
    for (size_t i = 0; i < outputNum; ++i) {
        void *buffer = nullptr;
        // modify size
        size_t size = aclmdlGetOutputSizeByIndex(modelDesc_.get(), i);
        APP_ERROR ret = aclrtMalloc(&buffer, size, policy);
        if (ret != APP_ERR_OK) {
            LogFatal << modelName_ << "model output aclrtMalloc fail(ret=" << ret
                     << "), buffer=" << buffer << ", size=" << size << ".";
            // Free the buffer malloced successfully before return error
            ReleaseModelBuffer(outputBuffers_);
            return ret;
        }
        outputBuffers_.push_back(buffer);
        outputSizes_.push_back(size);
        LogDebug << modelName_ << "model outputBuffer i=" << i << ", size=" << size << ".";
    }
    return APP_ERR_OK;
}

void ModelProcess::ReleaseModelBuffer(std::vector<void *> &modelBuffers) const
{
    for (size_t i = 0; i < modelBuffers.size(); i++) {
        if (modelBuffers[i] != nullptr) {
            aclrtFree(modelBuffers[i]);
            modelBuffers[i] = nullptr;
        }
    }
}

APP_ERROR ModelProcess::FreeDymInfoMem()
{
    switch (dynamicInfo_.dynamicType) {
    case STATIC_BATCH:
    case DYNAMIC_BATCH:
    case DYNAMIC_HW:
        break;
    case DYNAMIC_DIMS:
        if (dynamicInfo_.dyDims.pDims != nullptr){
            free(dynamicInfo_.dyDims.pDims);
            dynamicInfo_.dyDims.pDims = nullptr;
        }
        break;
    case DYNAMIC_SHAPE:
        if (dynamicInfo_.dyShape.pShapes != nullptr){
            free(dynamicInfo_.dyShape.pShapes);
            dynamicInfo_.dyShape.pShapes = nullptr;
        }
        break;
    }
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::SetModelStaticBatch() {
    dynamicInfo_.dynamicType = STATIC_BATCH;

    return APP_ERR_OK;
}

APP_ERROR ModelProcess::SetModelDynamicBatch(int batchsize) {
    dynamicInfo_.dyBatch.batchSize = batchsize;
    dynamicInfo_.dynamicType = DYNAMIC_BATCH;

    return APP_ERR_OK;
}

APP_ERROR ModelProcess::SetModelDynamicHW(uint64_t width, uint64_t height)
{
    dynamicInfo_.dyHW.width = width;
    dynamicInfo_.dyHW.height = height;
    dynamicInfo_.dynamicType = DYNAMIC_HW;

    return APP_ERR_OK;
}

void SplitStringSimple(std::string str, std::vector<std::string> &out, char split1, char split2, char split3)
{
    std::istringstream block(str);
    std::string cell;
    std::string cell1;
    std::string cell2;
    std::vector<std::string> split1_out;
    std::vector<std::string> split2_out;
    while (getline(block, cell, split1)) {
        split1_out.push_back(cell);
    }

    //find the last split2 because split2 only once
    for (auto var : split1_out){
        size_t pos = var.rfind(split2);
        if(pos != var.npos){
            split2_out.push_back(var.substr(pos + 1, var.size()-pos-1));
        }
    }

    for (size_t i = 0; i < split2_out.size(); ++i){
        std::istringstream block_tmp1(split2_out[i]);
        while (getline(block_tmp1, cell2, split3)) {
            out.push_back(cell2); 
        }
    }
}

void SplitStringWithPunctuation(std::string str, std::vector<std::string> &out, char split)
{
    std::istringstream block(str);
    std::string cell;
    while (getline(block, cell, split)) {
        out.push_back(cell);
    }
}

APP_ERROR SplitStingGetNameDimsMulMap(std::vector<std::string> in_dym_shape_str, std::map<std::string, int64_t> &out_namedimsmul_map)
{
    std::string name;
    std::string shape_str;
    
    for (size_t i = 0; i < in_dym_shape_str.size(); ++i){
        size_t pos = in_dym_shape_str[i].rfind(':');
        if(pos == in_dym_shape_str[i].npos){
            LogError << "find no : split i:" << i << " str: " << in_dym_shape_str[i].c_str();
            return APP_ERR_COMM_FAILURE;
        }
        name = in_dym_shape_str[i].substr(0, pos);
        shape_str = in_dym_shape_str[i].substr(pos + 1, in_dym_shape_str[i].size()-pos-1);

        std::vector<std::string> shape_tmp;
        SplitStringWithPunctuation(shape_str, shape_tmp, ',');
        int64_t DimsMul = 1;
        for(size_t j = 0; j < shape_tmp.size(); ++j){
            DimsMul = DimsMul * atoi(shape_tmp[j].c_str());
        }
        out_namedimsmul_map[name] = DimsMul;
    }
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::SetModelDynamicDims(std::string dymdimsStr)
{
    // Get dynamic dimension quantity
    CHECK_RET_EQ(GetDynamicGearCount(dym_gear_count_), APP_ERR_OK);

    LogInfo << "SetModelDynamicDims " << dymdimsStr;

    FreeDymInfoMem();

    if (dynamicInfo_.dyDims.pDims == nullptr){
        dynamicInfo_.dyDims.pDims = (DyDimsInfo *)calloc(1, sizeof(DyDimsInfo));
    }

    // How to release a dynamic array
    aclmdlIODims *dims = new aclmdlIODims[dym_gear_count_];
    SplitStringSimple(dymdimsStr, dynamicInfo_.dyDims.pDims->dym_dims, ';', ':', ',');

    if (dym_gear_count_ <= 0){
        LogInfo << "the dynamic_dims parameter is not specified for model conversion";
        delete [] dims;
        free(dynamicInfo_.dyDims.pDims);
        return APP_ERR_COMM_FAILURE;
    }

    APP_ERROR ret =  CheckDynamicDims(dynamicInfo_.dyDims.pDims->dym_dims, dym_gear_count_, dims);
    if (ret != APP_ERR_OK) {
        LogError << "check dynamic dims failed, please set correct dymDims paramenter";
        delete [] dims;
        free(dynamicInfo_.dyDims.pDims);
        return APP_ERR_COMM_FAILURE;
    }

    LogInfo << "prepare dynamic dims successful";

    // update realsize according real shapes
    std::vector<std::string> dymdims_tmp;
    SplitStringWithPunctuation(dymdimsStr, dymdims_tmp, ';'); 

    std::map<std::string, int64_t> namedimsmap;
    ret = SplitStingGetNameDimsMulMap(dymdims_tmp, namedimsmap);
    if (ret != APP_ERR_OK) {
        LogError << "split dims str failed";
        delete [] dims;
        free(dynamicInfo_.dyDims.pDims);
        return APP_ERR_COMM_FAILURE;
    }

    delete [] dims;

    dynamicInfo_.dynamicType = DYNAMIC_DIMS;
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::GetDynamicGearCount(size_t &dymGearCount)
{
    aclError ret; 
    ret = aclmdlGetInputDynamicGearCount(modelDesc_.get(), -1, &dymGearCount);
    if (ret != ACL_SUCCESS) {
        std::cout << aclGetRecentErrMsg() << std::endl;
        LogError << "get input dynamic gear count failed " << ret;
        return ret;
    }
    
    LogDebug << "get input dynamic gear count success";

    return APP_ERR_OK;
}

APP_ERROR ModelProcess::CheckDynamicDims(std::vector<std::string> dym_dims, size_t gearCount, aclmdlIODims *dims)
{
    aclmdlGetInputDynamicDims(modelDesc_.get(), -1, dims, gearCount);
    bool if_same = false;
    for (size_t i = 0; i < gearCount; i++)
    {
        if ((size_t)dym_dims.size() != dims[i].dimCount){
            LogError << "the dymDims parameter is not correct i: " << i << " dysize: " << dym_dims.size() << "dimcount: " << dims[i].dimCount;
            GetDimInfo(gearCount, dims);
            return APP_ERR_INFER_SET_INPUT_FAIL;
        }
        for (size_t j = 0; j < dims[i].dimCount; j++)
        {   
            if (dims[i].dims[j] != atoi(dym_dims[j].c_str()))
            {
                break;
            }
            if (j == dims[i].dimCount - 1)
            {
                if_same = true;
            }
        }
        
    }

    if(!if_same){
        LogError << "the dynamic_dims parameter is not correct";
        GetDimInfo(gearCount, dims);
        return APP_ERR_INFER_SET_INPUT_FAIL;  
    }
    LogInfo << "check dynamic dims success";
    return APP_ERR_OK;

}

APP_ERROR ModelProcess::SetDynamicShape(aclmdlDataset *input, std::map<std::string, std::vector<int64_t>> dym_shape_map, std::vector<int64_t> &dims_num)
{
    aclError ret;
    const char *name;
    size_t input_num = dym_shape_map.size();
    aclTensorDesc * inputDesc;
    for (size_t i = 0; i < input_num; i++) {
        name = aclmdlGetInputNameByIndex(modelDesc_.get(), i);
        int64_t arr[dym_shape_map[name].size()];
        std::copy(dym_shape_map[name].begin(), dym_shape_map[name].end(), arr);
        inputDesc = aclCreateTensorDesc(ACL_FLOAT, dims_num[i], arr, ACL_FORMAT_NCHW);
        ret = aclmdlSetDatasetTensorDesc(input, inputDesc, i);
        if (ret != ACL_SUCCESS) {
            std::cout << aclGetRecentErrMsg() << std::endl;
            LogError << "aclmdlSetDatasetTensorDesc failed " << ret;
            return ret;
        }
    }
    LogDebug << "set Dynamic shape success";
    return APP_ERR_OK;  
}

APP_ERROR ModelProcess::SetDynamicHW(aclmdlDataset *input, size_t dymindex, uint64_t height, uint64_t width)
{
    aclError ret;
    ret = aclmdlSetDynamicHWSize(modelId_, input, dymindex, height, width);
    if (ret != ACL_SUCCESS) {
        std::cout << aclGetRecentErrMsg() << std::endl;
        LogError << "aclmdlSetDynamicHWSize failed " << ret;
        return ret;
    }
    LogDebug << "set Dynamic HW success";
    return APP_ERR_OK;
}

APP_ERROR ModelProcess::SetDynamicBatchSize(aclmdlDataset *input, size_t dymindex, uint64_t batchSize)
{
    aclError ret = aclmdlSetDynamicBatchSize(modelId_, input, dymindex, batchSize);
    if (ret != ACL_SUCCESS) {
        std::cout << aclGetRecentErrMsg() << std::endl;
        LogError << "aclmdlSetDynamicBatchSize failed " << ret;
        return ret;
    }
    LogDebug << "set dynamic batch size success";
    return APP_ERR_OK; 
}

APP_ERROR ModelProcess::SetDynamicDims(aclmdlDataset *input, size_t dymindex, std::vector<std::string> dym_dims)
{   
    aclmdlIODims dims;
    dims.dimCount = dym_dims.size();
    for (size_t i = 0; i < dims.dimCount; i++)
    {   
        dims.dims[i] = atoi(dym_dims[i].c_str());
    }

    aclError ret = aclmdlSetInputDynamicDims(modelId_, input, dymindex, &dims);
    if (ret != APP_ERR_OK) {
        std::cout << aclGetRecentErrMsg() << std::endl;
        LogError << "aclmdlSetInputDynamicDims failed " << ret;
        return ret;
    }
    LogDebug << "set dynamic dims success";
    return APP_ERR_OK; 
}

void ModelProcess::GetDymBatchInfo(void)
{
    aclmdlBatch batch_info;
    aclmdlGetDynamicBatch(modelDesc_.get(), &batch_info);
    std::stringstream ss;
    ss << "model has dynamic batch size:{";
    for (size_t i = 0; i < batch_info.batchCount; i++) {
        ss << "[" << batch_info.batch[i] << "]";  
    }
    ss << "}, please set correct dynamic batch size";
    LogError << ss.str().c_str(); 
}

void ModelProcess::GetDymHWInfo(void)
{
    aclmdlHW  hw_info;
    aclmdlGetDynamicHW(modelDesc_.get(), -1, &hw_info);
    std::stringstream ss;
    
    LogError << "model has " << hw_info.hwCount << " gear of HW";
    for (size_t i = 0; i < hw_info.hwCount; i++) {
        ss << "[" << hw_info.hw[i] << "]";  
    }
    ss << "}, please set correct dynamic batch size";
    LogError << ss.str().c_str(); 
}

void ModelProcess::GetDimInfo(size_t gearCount, aclmdlIODims *dims)
{
    aclmdlGetInputDynamicDims(modelDesc_.get(), -1, dims, gearCount);

    for (size_t i = 0; i < gearCount; i++)
    {
        if (i == 0)
        {
            LogError << "model has " << gearCount << " gear of dims"; 
        }
        std::stringstream ss;
        ss << "dims[" << i << "]:";
        for (size_t j = 0; j < dims[i].dimCount; j++)
        {
            ss << "[" << dims[i].dims[j] << "]";  
        }
        LogError << ss.str().c_str(); 
    }
}