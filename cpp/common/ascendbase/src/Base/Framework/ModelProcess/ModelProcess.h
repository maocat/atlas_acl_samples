/*
 * Copyright(C) 2020. Huawei Technologies Co.,Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MODELPROCSS_H
#define MODELPROCSS_H

#include <cstdio>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <unordered_map>
#include <mutex>
#include "acl/acl.h"
#include "CommonDataType/CommonDataType.h"
#include "Log/Log.h"
#include "ErrorCode/ErrorCode.h"
#ifdef USE_DCMI_INTERFACE
#include "dcmi_interface_api.h"
#endif

#define CHECK_RET_EQ(func, expect_value) \
{ \
auto ret = (func); \
if (ret != expect_value) { \
    LogWarn << "Check failed:" << #func << "ret: " << ret; \
    return ret; \
} \
}

enum DataFormat {
    NCHW = 0,
    NHWC = 1
};

enum DynamicType {
    STATIC_BATCH = 0,
    DYNAMIC_BATCH = 1,
    DYNAMIC_HW = 2,
    DYNAMIC_DIMS = 3,
    DYNAMIC_SHAPE = 4,
};

struct DyDimsInfo {
    std::vector<std::string> dym_dims;
};

struct DyShapeInfo {
    std::vector<int64_t> dims_num;
    std::map<std::string, std::vector<int64_t>> dym_shape_map;
};

struct DynamicInfo {
    DynamicType dynamicType = STATIC_BATCH;
    union{
        struct {
            uint64_t batchSize;
        }staticBatch;
        struct {
            uint64_t batchSize;
            uint64_t maxbatchsize;
        }dyBatch;
        struct {
            uint64_t height;
            uint64_t width;
            uint64_t maxHWSize;
        }dyHW;
        struct {
            DyDimsInfo* pDims;
        }dyDims;
        struct {
            DyShapeInfo* pShapes;
        }dyShape;
    };
};

// Class of model inference
class ModelProcess {
public:

    // Construct a new Model Process object for model in the device
    ModelProcess(const int deviceId, const std::string& modelName);
    ModelProcess();
    ~ModelProcess();

    int Init(std::string modelPath);
#ifdef USE_DCMI_INTERFACE
    int Init(const std::string &modelPath, bool isEncrypted, int cardId = 0, int deviceId = 0);
#endif
    int DeInit();

    APP_ERROR InputBufferWithSizeMalloc(aclrtMemMallocPolicy policy = ACL_MEM_MALLOC_HUGE_FIRST);
    APP_ERROR OutputBufferWithSizeMalloc(aclrtMemMallocPolicy policy = ACL_MEM_MALLOC_HUGE_FIRST);
    int ModelInference(const std::vector<void *> &inputBufs, const std::vector<size_t> &inputSizes,
                       const std::vector<void *> &ouputBufs, const std::vector<size_t> &outputSizes);
    aclmdlDesc *GetModelDesc() const;
    size_t GetModelNumInputs() const;
    size_t GetModelNumOutputs() const;
    size_t GetModelInputSizeByIndex(const size_t &i) const;
    APP_ERROR GetDynamicIndex(size_t &dymindex);
    size_t GetModelOutputSizeByIndex(const size_t &i) const;
    void ReleaseModelBuffer(std::vector<void *> &modelBuffers) const;

    APP_ERROR SetModelStaticBatch();
    APP_ERROR SetModelDynamicBatch(int batchsize);
    APP_ERROR SetModelDynamicHW(uint64_t width, uint64_t height);
    APP_ERROR SetModelDynamicDims(std::string dymdimsStr);

    APP_ERROR GetDynamicGearCount(size_t &dymGearCount);
    APP_ERROR CheckDynamicDims(std::vector<std::string> dym_dims, size_t gearCount, aclmdlIODims *dims);
    void GetDymBatchInfo(void);
    void GetDymHWInfo(void);
    void GetDimInfo(size_t gearCount, aclmdlIODims *dims);

    std::vector<void *> inputBuffers_ = {};
    std::vector<size_t> inputSizes_ = {};
    std::vector<void *> outputBuffers_ = {};
    std::vector<size_t> outputSizes_ = {};

private:
    aclmdlDataset *CreateAndFillDataset(const std::vector<void *> &bufs, const std::vector<size_t> &sizes) const;
    void DestroyDataset(const aclmdlDataset *dataset) const;
    APP_ERROR LoadModel(const std::shared_ptr<uint8_t> &modelData, int modelSize);
#ifdef USE_DCMI_INTERFACE
    static void SetConsoleDispMode(int fd, int option);
    APP_ERROR GetKeyIdPassword(unsigned int &id, unsigned char password[], unsigned int &passwordLen) const;
#endif
    APP_ERROR FreeDymInfoMem();

    APP_ERROR SetDynamicInfo(aclmdlDataset *input);
    APP_ERROR SetDynamicShape(aclmdlDataset *input, std::map<std::string, std::vector<int64_t>> dym_shape_map, std::vector<int64_t> &dims_num);
    APP_ERROR SetDynamicHW(aclmdlDataset *input, size_t dymindex, uint64_t height, uint64_t width);
    APP_ERROR SetDynamicBatchSize(aclmdlDataset *input, size_t dymindex, uint64_t batchSize);
    APP_ERROR SetDynamicDims(aclmdlDataset *input, size_t dymindex, std::vector<std::string> dym_dims);

    std::mutex mtx_ = {};
    int deviceId_ = 0; // Device used
    std::string modelName_ = "";
    uint32_t modelId_ = 0; // Id of import model
    uint32_t modelWidth_ = 0;
    uint32_t modelHeight_ = 0;
    void *modelDevPtr_ = nullptr;
    size_t modelDevPtrSize_ = 0;
    void *weightDevPtr_ = nullptr;
    size_t weightDevPtrSize_ = 0;
    aclrtContext contextModel_ = nullptr;
    std::shared_ptr<aclmdlDesc> modelDesc_ = nullptr;
    bool isDeInit_ = false;
#ifdef USE_DCMI_INTERFACE
    DCMI_ENCRYPTED_DATA_NODE encryptModelData_ = {}; // information for encrypted model
#endif
    size_t g_dymindex = -1;
    DynamicInfo dynamicInfo_ = {};
    aclmdlDataset *input_;
    size_t dym_gear_count_;
};

#endif
