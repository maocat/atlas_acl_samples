/*
 * Copyright (c) 2020.Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UTILS_H
#define UTILS_H

#include <mutex>
#include <sstream>
#include <string>
#include <vector>
#include "acl/acl.h"
#include "CommonDataType/CommonDataType.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#define RGB_IMAGE_SIZE_F32(width, height) ((width) * (height)*3 * 4)
#define GRAY_IMAGE_SIZE_F32(width, height) ((width) * (height)*1 * 4)
#define IMAGE_CHAN_SIZE_F32(width, height) ((width) * (height)*4)

// Class for text object detection
class __attribute__((visibility("default"))) TextObjectInfo {
public:
    float x0;
    float y0;
    float x1;
    float y1;
    float x2;
    float y2;
    float x3;
    float y3;
    float confidence;
    std::string result;
};

// Class for resized image info
class __attribute__((visibility("default"))) ResizedImageInfo {
public:
    uint32_t widthResize;    // memoryWidth
    uint32_t heightResize;   // memoryHeight
    uint32_t widthOriginal;  // imageWidth
    uint32_t heightOriginal; // imageHeight
};

// Class for text generation (i.e. translation, OCR)
class __attribute__((visibility("default"))) TextsInfo {
public:
    std::vector<std::string> text;
};

/**
 * Utils
 */
class Utils {
public:
    static uint8_t *ImageNchw(std::vector<cv::Mat> &nhwcImageChs, uint32_t size);

    static void *CopyDataToDevice(void *data, uint32_t dataSize, aclrtMemcpyKind policy);
    static void *CopyDataDeviceToDevice(void *deviceData, uint32_t dataSize);
    static void *CopyDataHostToDevice(void *deviceData, uint32_t dataSize);
};

#endif