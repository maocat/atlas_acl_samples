/*
 * Copyright (c) 2020.Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CrnnPost.h"

#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <stack>
#include "Log/Log.h"

CrnnPost::CrnnPost(void) {}

void CrnnPost::ClassNameInit(const std::string &fileName)
{
    std::ifstream fsIn(fileName);
    std::string line;

    if (fsIn) { // File exists
        labelVec_.clear();
        while (getline(fsIn, line)) {
            labelVec_.push_back(line);
        }
    } else {
        LogInfo << "no such file";
    }

    fsIn.close();

    classNum_ = labelVec_.size();

    LogInfo << " ClassNameInit classNum_ " << classNum_;

    return;
}

std::string CrnnPost::GetClassName(const size_t classId)
{
    if (classId >= labelVec_.size()) {
        LogError << "Failed to get classId(" << classId << ") label, size(" << labelVec_.size() << ").";
        return "";
    }
    LogInfo << "GetClassName " << labelVec_[classId];
    return labelVec_[classId];
}

std::string CrnnPost::CalcOutputArgmax(std::shared_ptr<void> &crnnResult, size_t outputSize)
{
    LogDebug << "Start to Process CalcOutputArgmax.";

    float *outputInfo = (float *)(crnnResult.get());
    if (outputInfo == nullptr) {
        LogError << "outputInfo is nullptr";
        return "";
    }

    objectNum_ = outputSize / classNum_ / sizeof(float);
    LogDebug << "CalcOutputArgmax objectNum: " << objectNum_;

    std::vector<float> logits;
    uint32_t previousIdx = blankIdx_;
    std::string result = "";
    for (uint32_t i = 0; i < objectNum_; i++) {
        for (uint32_t j = 0; j < classNum_; j++) {
            logits.push_back(outputInfo[i * classNum_ + j]);
        }
        auto maxElement = std::max_element(std::begin(logits), std::end(logits));
        uint32_t argmaxIndex = (uint32_t)(maxElement - std::begin(logits));
        if (argmaxIndex != blankIdx_ && argmaxIndex != previousIdx) {
            result += GetClassName(argmaxIndex);
        }
        previousIdx = argmaxIndex;
        logits.clear();
    }

    LogDebug << "End to Process CalcOutputArgmax.";
    return result;
}

void CrnnPost::TextGenerationOutput(const std::string &fileName, std::vector<std::shared_ptr<void>> &singleResult,
        std::vector<size_t> &outputSizes, std::vector<TextsInfo> &textsInfos)
{
    LogDebug << "CrnnPostProcess start to write results.";
    uint32_t batchSize = singleResult.size();

    ClassNameInit(fileName);

    for (uint32_t i = 0; i < batchSize; i++) {
        TextsInfo textsInfo;
        std::string result = "";
        result = CalcOutputArgmax(singleResult[i], outputSizes[i]);
        textsInfo.text.push_back(result);
        textsInfos.push_back(textsInfo);
        LogInfo << "CrnnPostProcess output string(" << textsInfos[i].text[0] << ").";
    }
    LogDebug << "CrnnPostProcess end to write results.";
}