EN|[CN](README.zh.md)
# MultiVideoDecode

## Introduction

This sample demonstrates how the MultiVideoDecode program to use the chip to video decoder.

Process Framework:

```
StreamPuller > VideoDecoder > WriteResult
```

## Supported Products

Atlas 800 (Model 3000), Atlas 800 (Model 3010), Atlas 300 (Model 3010), Atlas 500 (Model 3010), Atlas 300I (Model 6000)

## Supported ACL Version

1.73.5.1.B050, 1.73.5.2.B050, 1.75.T11.0.B116, 20.1.0, 20.2.0

Run the following command to check the version in the environment where the Atlas product is installed:
```bash
npu-smi info
```

## Dependency

Code dependency:

This sample depends on the ascendbase directory.

If the whole package is not copied, ensure that the ascendbase and MultiVideoDecode directories are copied to the same directory in the compilation environment. Otherwise, the compilation will fail. If the whole package is copied, ignore it.

Set the environment variable:
*  `ASCEND_HOME`      Ascend installation path, which is generally `/usr/local/Ascend`
*  `LD_LIBRARY_PATH`  Specifies the dynamic library search path on which the sample program depends

```bash
export ASCEND_HOME=/usr/local/Ascend
export LD_LIBRARY_PATH=$ASCEND_HOME/ascend-toolkit/latest/acllib/lib64:$LD_LIBRARY_PATH
```

#### FFmpeg 4.2

Source download address: https://github.com/FFmpeg/FFmpeg/releases

To compile and install ffmpeg with source code, you can refer to Ascend developers BBS: https://bbs.huaweicloud.com/blogs/140860

If cross compilation is required, go to the directory of the ffmpeg source code package and run the following command to complete cross compilation. Specify the installation path in the --prefix option.

```bash
export PATH=${PATH}:${ASCEND_HOME}/ascend-toolkit/latest/toolkit/toolchain/hcc/bin
./configure --prefix=/your/specify/path --target-os=linux --arch=aarch64 --enable-cross-compile --cross-prefix=aarch64-target-linux-gnu- --enable-shared --disable-doc --disable-vaapi --disable-libxcb --disable-libxcb-shm --disable-libxcb-xfixes --disable-libxcb-shape --disable-asm
make -j
make install -j
```

Config FFMPEG4.2 library, set FFMPEG environment variable, for example FFMPEG install path is "/usr/local/ffmpeg". If cross compilation is used, set this parameter to the ffmpeg installation path during cross compilation.
```bash
export FFMPEG_PATH=/usr/local/ffmpeg
export LD_LIBRARY_PATH=$FFMPEG_PATH/lib:$LD_LIBRARY_PATH
```

## configuration

Configure the device_id, model_path and model input format in `data/config/setup.config`

Configure device_id
```bash
#chip config
device_id = 0 #use the device to run the program
```

Configure stream number and stream path

Configure stream number
```bash
#Stream number
stream_num = 4
```
Configure stream path, support rtsp video stream or local video file
Configure rtsp video stream path, the format is as follows:
```bash
stream.ch0 = rtsp://xx.xx.xx.xx:xx/xxx.264
stream.ch1 = rtsp://xx.xx.xx.xx:xx/xxx.264
stream.ch2 = rtsp://xx.xx.xx.xx:xx/xxx.264
stream.ch3 = rtsp://xx.xx.xx.xx:xx/xxx.264
```
Configure local video file path, the format is as follows:
```bash
stream.ch0 = ./videos/0_720P_25.264
stream.ch1 = ./videos/1_720P_25.264
stream.ch2 = ./videos/2_720P_25.264
stream.ch3 = ./videos/3_720P_25.264
```

Configure resize width and height
```bash
VideoDecoder.resizeWidth = 416    # must be equal to ModelInfer.modelWidth
VideoDecoder.resizeHeight = 416   # must be equal to ModelInfer.modelHeight
```

## Compilation

Compile Atlas 800 (Model 3000), Atlas 800 (Model 3010), Atlas 300 (Model 3010) programs
```bash
bash build.sh
```

Compile Atlas 500 (Model 3010) program
Run the following command on the ARM server
```bash
bash build.sh
```

Run the following command on the X86 server
```bash
bash build.sh A500
```

If you want to run with the compilation result on another environment, copy the dist directory and the ffmpeg dynamic libraries.

## Execution


For help
```bash
cd dist
./main -h

------------------------------help information------------------------------
-acl_setup                    ./data/config/acl.json        the config file using for AscendCL init.
-debug_level                  1                             debug level:0-debug, 1-info, 2-warn, 3-error, 4-fatal, 5-off.
-h                            help                          show helps
-help                         help                          show helps
-setup                        ./data/config/setup.config    the config file using for face recognition pipeline
```

Object detection for the video stream
```bash
cd dist
./main
```

## Constraint

Support input format: H264 or H265



## Result
Print the decoded information on the screen.
